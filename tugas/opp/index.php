<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Nama : " . $sheep->get_name() . "<br>"; // "shaun"
echo "legs : " . $sheep->get_legs() . "<br>"; // 4
echo "cold blooded : " . $sheep->get_cold_blooded() . "<br><br>"; // "no"

$bdk = new frog("buduk");
echo "Nama : " . $bdk->get_name() . "<br>"; // "shaun"
echo "legs : " . $bdk->get_legs() . "<br>"; // 4
echo "cold blooded : " . $bdk->get_cold_blooded() . "<br>"; // "no"
echo "Jump : " . $bdk->jump() . "<br><br>";

$ks = new ape("Kera Sakti");
echo "Nama : " . $ks->get_name() . "<br>"; // "shaun"
echo "legs : " . $ks->get_legs() . "<br>"; // 4
echo "cold blooded : " . $ks->get_cold_blooded() . "<br>"; // "no"
echo "Jump : " . $ks->Yell() . "<br>";
