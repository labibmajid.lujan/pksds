@extends('layouts.dashboard-volt')

@section('css')

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Pemain Cast</div>
                <div class="col-md-3 mb-3">
                    <a href="/cast/create" type="button" class="btn btn-primary btn-flat">Add</a>
                </div>
                <div class="card-body">
                    @if (session('pesan'))
                    <div class="alert alert-primary d-flex align-items-center" role="alert">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                        </svg>
                        <div>
                            {{session('pesan')}}
                        </div>
                    </div>
                    @endif
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th width="40px">No</th>
                                <th>Name</th>
                                <th width="40px">Umur</th>
                                <th>Bio</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach ($casts as $cs)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $cs->name}}</td>
                                <td>{{ $cs->umur}}</td>
                                <td>{{ $cs->bio}}</td>
                                <td>
                                    <a href="/cast/{{$cs->id}}/edit">Edit</a>
                                    <form action="/cast/{{$cs->id}}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="Delete">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')

@endpush