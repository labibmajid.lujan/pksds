<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('cast.cast', compact(['casts']));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // dd($request->except(['_token', 'submit']));
        Cast::create($request->except(['_token', 'submit']));
        return redirect('/cast');
    }

    public function edit($id)
    {
        $casts = Cast::find($id);
        return view('cast.edit', compact(['casts']));
    }

    public function update($id, Request $request)
    {
        $casts = Cast::find($id);
        $casts->update($request->except(['_token', 'submit']));
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $casts = Cast::find($id);
        $casts->delete();
        return redirect('/cast');
    }
}
