<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\SekolahController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/tugashome', [App\Http\Controllers\HomeController::class, 'tugashome'])->name('tugashome');
    Route::get('/tugasregister', [App\Http\Controllers\BiodataController::class, 'tugasregister'])->name('tugasregister');
    Route::post('/tugaswelcome', [App\Http\Controllers\BiodataController::class, 'tugaswelcome'])->name('tugaswelcome');

    //route cast
    route::get('/cast', [\App\Http\Controllers\CastController::class, 'index'])->name('cast');
    Route::get('/cast/create', [CastController::class, 'create'])->name('create');
    Route::post('/cast/{id}', [CastController::class, 'store'])->name('store');
    Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->name('edit');
    Route::put('/cast/{id}', [CastController::class, 'update'])->name('update');
    Route::delete('/cast/{id}', [CastController::class, 'destroy'])->name('destroy');

    //sekolah dan map 
    Route::get('/sekolah-map', [App\Http\Controllers\HomeController::class, 'sekolah_map'])->name('sekolah-map');
    Route::get('/kecamatan', [App\Http\Controllers\KecamatanController::class, 'index'])->name('kecamatan');
    Route::get('/kecamatan/add', [KecamatanController::class, 'add']);
    Route::post('/kecamatan/insert', [KecamatanController::class, 'insert']);
    Route::get('/kecamatan/edit/{id}', [KecamatanController::class, 'edit']);
    Route::post('/kecamatan/update/{id}', [KecamatanController::class, 'update']);
    Route::get('/kecamatan/delete/{id}', [KecamatanController::class, 'delete']);

    //sekolah
    Route::get('sekolah', [SekolahController::class, 'index'])->name('sekolah');
    Route::get('sekolah/add', [SekolahController::class, 'add']);
    Route::post('sekolah/insert', [SekolahController::class, 'insert']);
    Route::get('sekolah/edit/{id}', [SekolahController::class, 'edit']);
    Route::post('sekolah/update/{id}', [SekolahController::class, 'update']);
    Route::get('sekolah/delete/{id}', [SekolahController::class, 'delete']);
});
