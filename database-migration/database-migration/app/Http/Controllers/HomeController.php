<?php

namespace App\Http\Controllers;

use App\Models\HomeModel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $HomeModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->HomeModel = new HomeModel();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function tugashome()
    {
        return view('tugas.home');
    }

    public function sekolah_map()
    {
        $data = [
            'kecamatan' => $this->HomeModel->DataKecamatan(),   //menampilkan batas kecamatan pada map donatur
            'sekolah' => $this->HomeModel->SemuaDataSekolah(), //memanggil agar data donatur bisa tampil menggunakan marker pada map
        ];
        return view('sekolah-map', $data);
    }
}
