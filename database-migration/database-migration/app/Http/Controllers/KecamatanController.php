<?php

namespace App\Http\Controllers;

use App\Models\KecamatanModel;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    private $KecamatanModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->KecamatanModel = new KecamatanModel();
    }

    public function index()
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData()
        ];
        return view('kecamatan.indexkec', $data);
    }

    public function add()
    {
        $data = [];
        return view('kecamatan.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'kecamatan' => 'required',
                'warna' => 'required',
                'geojson' => 'required',
            ],
            [
                'kecamatan.required' => 'wajib diisi !!!',
                'warna.required' => 'wajib diisi !!!',
                'geojson.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_kecamatan' => Request()->kecamatan,
            'warna' => Request()->warna,
            'geojson' => Request()->geojson,
        ];

        //menyimpan ke modelkecamatan
        $this->KecamatanModel->InserData($data);
        return redirect()->route('kecamatan')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->DetailData($id),
        ];
        return view('kecamatan.edit', $data);
    }

    public function update($id)
    {
        request()->validate(
            [
                'nama_kecamatan' => 'required',
                'warna' => 'required',
                'geojson' => 'required',
            ],
            [
                'nama_kecamatan.required' => 'wajib diisi !!!',
                'warna.required' => 'wajib diisi !!!',
                'geojson.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_kecamatan' => Request()->nama_kecamatan,
            'warna' => Request()->warna,
            'geojson' => Request()->geojson,
        ];

        $this->KecamatanModel->UpdateData($id, $data);
        return redirect()->route('kecamatan')->with('pesan', 'Data Berhasil ter-update');
    }

    public function delete($id)
    {
        $this->KecamatanModel->DeleteData($id);
        return redirect()->route('kecamatan')->with('pesan', 'Data Berhasil ter-delete');
    }
}
