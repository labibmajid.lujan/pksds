<?php

namespace App\Http\Controllers;

use App\Models\KecamatanModel;
use App\Models\SekolahModel;
use Illuminate\Http\Request;

class SekolahController extends Controller
{
    private $SekolahModel;
    private $KecamatanModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->SekolahModel = new SekolahModel();
        $this->KecamatanModel = new KecamatanModel();
    }

    public function index()
    {
        $data = [
            'sekolah' => $this->SekolahModel->AllData(),
        ];
        return view('sekolah.indexs', $data);
    }

    public function add()
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData(),    //use data kecamatan
        ];
        return view('sekolah.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'nama_sekolah' => 'required',
                'id_kecamatan' => 'required',
                'alamat' => 'required',
                'posisi' => 'required',
                'keterangan' => 'required',
            ],
            [
                'nama_sekolah.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'alamat.required' => 'wajib diisi !!!',
                'posisi.required' => 'wajib diisi !!!',
                'keterangan.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_sekolah' => Request()->nama_sekolah,
            'id_kecamatan' => Request()->id_kecamatan,
            'alamat' => Request()->alamat,
            'posisi' => Request()->posisi,
            'keterangan' => Request()->keterangan,
        ];

        //menyimpan ke modelkecamatan
        $this->SekolahModel->InserData($data);
        return redirect()->route('sekolah')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData(),
            'sekolah' => $this->SekolahModel->DetailData($id),
        ];
        return view('sekolah.edit', $data);
    }

    public function update($id)
    {
        request()->validate(
            [
                'nama_sekolah' => 'required',
                'id_kecamatan' => 'required',
                'alamat' => 'required',
                'posisi' => 'required',
                'keterangan' => 'required',
            ],
            [
                'nama_sekolah.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'alamat.required' => 'wajib diisi !!!',
                'posisi.required' => 'wajib diisi !!!',
                'keterangan.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_sekolah' => Request()->nama_sekolah,
            'id_kecamatan' => Request()->id_kecamatan,
            'alamat' => Request()->alamat,
            'posisi' => Request()->posisi,
            'keterangan' => Request()->keterangan,
        ];
        $this->SekolahModel->UpdateData($id, $data);
        return redirect()->route('sekolah')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id)
    {
        $this->SekolahModel->DeleteData($id);
        return redirect()->route('sekolah')->with('pesan', 'Data Berhasil ter-delete');
    }
}
