<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HomeModel extends Model
{
    public function DataKecamatan()
    {
        return DB::table('kecamatan')
            ->get();
    }

    public function SemuaDataSekolah() //memanggil agar data donatur bisa tampil menggunakan marker pada map
    {
        return DB::table('sekolah')
            //fungsi join
            ->join('kecamatan', 'kecamatan.id', '=', 'sekolah.id_kecamatan')
            // ->join('kecamatan', 'kecamatan.id', '=', 'sekolah.id')
            ->get();
    }
}
