<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KecamatanModel extends Model
{
    public function AllData()
    {
        return DB::table('kecamatan')
            ->get();
    }

    public function InserData($data)
    {
        DB::table('kecamatan')
            ->insert($data);
    }

    //ambil data untuk diedit
    public function DetailData($id)
    {
        return DB::table('kecamatan')
            ->where('id', $id)
            ->first(); //berdasarkan idkecamatan
    }

    public function UpdateData($id, $data)
    {
        DB::table('kecamatan')
            ->where('id', $id)
            ->update($data);
    }


    public function DeleteData($id)
    {
        DB::table('kecamatan')
            ->where('id', $id)
            ->delete();
    }
}
