<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SekolahModel extends Model
{
    public function AllData()
    {
        return DB::table('sekolah')
            //fungsi join
            ->join('kecamatan', 'kecamatan.id', '=', 'sekolah.id_kecamatan')
            ->get();
    }

    public function InserData($data)
    {
        DB::table('sekolah')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id)
    {
        return DB::table('sekolah')
            ->join('kecamatan', 'kecamatan.id', '=', 'sekolah.id_kecamatan')
            ->where('id_kecamatan', $id)
            ->first(); //berdasarkan id_donatur
    }

    public function UpdateData($id, $data)
    {
        DB::table('sekolah')
            ->where('id', $id)
            ->update($data);
    }

    public function DeleteData($id)
    {
        DB::table('sekolah')
            ->where('id', $id)
            ->delete();
    }
}
