@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4">Edit Data Kecamatan</h2>
                <form action="/kecamatan/update/{{$kecamatan->id}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name">Kecamatan</label>
                                <!-- value mem-passing data dari model/controller -->
                                <input class="form-control" name="nama_kecamatan" value="{{$kecamatan->nama_kecamatan}}" id="first_name" type="text">
                                <div class="text-danger">
                                    @error('kecamatan')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="exampleColorInput" class="form-label">Warna</label>
                            <input type="color" name="warna" value="{{$kecamatan->warna}}" class="form-control form-control-color" id="exampleColorInput" value="#563d7c" title="Choose your color">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <div class="form-group">
                                <label for="address">GeoJson</label>
                                <textarea name="geojson" id="example" class="form-control" rows="4">{{$kecamatan->geojson}}</textarea>
                            </div>
                        </div>
                        <div class="text-danger">
                            @error('geojson')
                            {{$message}}
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">Save all</button>
                            <a href="/kecamatan/add" type="button" class="btn btn-gray-800 mt-2 animate-up-2">Kembali</a>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>
@endsection