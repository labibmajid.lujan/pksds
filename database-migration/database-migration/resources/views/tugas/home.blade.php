@extends('layouts.dashboard-volt')

@section('css')

@endsection

@section('content')
<div class="card-body">
    <h1>Media Online</h1>

    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

    <h3>Benefit Join di Media Online</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon Developer terbaik</li>
    </ul>

    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftarkan di <a href="tugasregister">Sign Up Disini</a></li>
        <li>Selesai</li>
    </ol>
</div>
@endsection

@push('javascript')

@endpush