@extends('layouts.dashboard-volt')

@section('css')

@endsection

@section('content')
<div class="card-body">
    <h1>Buat Akun Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="{{route('tugaswelcome')}}" method="post">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="name1"><br><br>

        <label>Last name:</label><br>
        <input type="text" name="name2"><br><br>

        <label>Gender</label><br>
        <input type="radio">Male<br>
        <input type="radio">Femele<br><br>

        <label>Nationality</label><br>
        <select name="Negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option>
            <option value="Brunai Darussalam">Brunai Darussalam</option>
        </select><br><br>

        <label>Language Spoken</label><br>
        <input type="checkbox" name="Language">Bahasa Indonesia<br>
        <input type="checkbox" name="Language">English<br>
        <input type="checkbox" name="Language">Other<br><br>

        <label>Bio</label><br>
        <textarea name="massage" rows="10" cols="30"></textarea><br>
        <input type="submit">
    </form>
</div>
@endsection

@push('javascript')
<!-- <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    <script>
        const map = L.map('map').setView([-5.129541583080711, 113.62957770241515], 4);

        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        var iconMarker = L.icon({
            iconUrl:'{{ asset('iconMarkers/marker.png') }}',
            iconSize:[50,50],
        })

        var marker = L.marker([-5.129541583080711, 113.62957770241515],{
            icon:iconMarker,
            draggable:true
        })
        .bindPopup('Tampilan pesan disini 1')
        .addTo(map);

        var marker2 = L.marker([-1.2761076471197752, 116.82459558367206],{
            //icon:iconMarker,
            draggable:true
        })
        .bindPopup('Tampilan pesan disini 2')
        .addTo(map);

         -->
</script>
@endpush