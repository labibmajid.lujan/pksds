@extends('layouts.dashboard-volt')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />

<style>
    #map {
        height: 400px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Donatur map</div>
                <div class="card-body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('javascript')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"> </script>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });


    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    // var peta4 = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //     maxZoom: 19,
    //     attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    // });


    var kecamatan = L.layerGroup(); //menambahkan layer kecamatan
    var sekolah = L.layerGroup(); //menambahkan layer donatur

    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 12,
        layers: [peta2, kecamatan, sekolah]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
        // "Biasa": peta4
    };

    //menampilkan tombol checbox kecamatan, donatur pada layer
    var overlayer = {
        "Kecamatan": kecamatan,
        "Sekolah": sekolah,
    };

    L.control.layers(baseMaps, overlayer).addTo(map);


    //menampilkan batas kecamatan dengan mengambil data dari fungsi donatur_map pada HomeController
    @foreach($kecamatan as $data)
    L.geoJSON(<?= $data->geojson ?>, {
        style: {
            color: 'white', //color samping
            fillColor: '{{$data->warna}}', //color isi
            fillOpacity: 0.1,
        }
    }).addTo(kecamatan).bindPopup("{{$data->nama_kecamatan}}");
    @endforeach

    @foreach($sekolah as $data)
    L.marker([<?= $data->posisi ?>]).addTo(sekolah).bindPopup("{{$data->nama_sekolah}}");
    @endforeach
</script>


@endpush